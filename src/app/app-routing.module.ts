/**
 @Author  Vinu Sagar
 Licensed to Notyfyd.com*/

import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {GradesComponent} from './components/grades/grades.component';
import {FGradesComponent} from './components/f-grades/f-grades.component';
import {DynamicStepperComponent} from './components/dynamic-stepper/dynamic-stepper.component';
import {DynamicCheckboxComponent} from './components/dynamic-checkbox/dynamic-checkbox.component';
import {FeeStructureComponent} from './components/fee-structure/fee-structure.component';
import {FeeInstallmentsComponent} from './components/fee-installments/fee-installments.component';
const routes: Routes = [
      {path: 'grades', component: GradesComponent},
      {path: 'fgrades', component: FGradesComponent},
      {path:'stepper', component:DynamicStepperComponent},
      {path:'chbox', component:DynamicCheckboxComponent},
      {path:'fee', component:FeeStructureComponent},
      {path:'installments', component: FeeInstallmentsComponent}
      
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{}

