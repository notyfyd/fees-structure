import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { GradesComponent } from './components/grades/grades.component';
import {
      MatButtonModule,
      MatCardModule,
      MatCheckboxModule, MatDatepickerModule,
      MatFormFieldModule, MatIconModule,
      MatInputModule,
      MatRadioModule,
      MatSelectModule, MatSortModule, MatStepperModule, MatTableModule, MatToolbarModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FGradesComponent } from './components/f-grades/f-grades.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import { TesterComponent } from './components/tester/tester.component';
import { DynamicStepperComponent } from './components/dynamic-stepper/dynamic-stepper.component';
import { DynamicCheckboxComponent } from './components/dynamic-checkbox/dynamic-checkbox.component';
import { FeeStructureComponent } from './components/fee-structure/fee-structure.component';
import { FeeInstallmentsComponent } from './components/fee-installments/fee-installments.component';
import {MatMomentDateModule} from '@angular/material-moment-adapter';

@NgModule({
  declarations: [
    AppComponent,
    GradesComponent,
    FGradesComponent,
    TesterComponent,
    DynamicStepperComponent,
    DynamicCheckboxComponent,
    FeeStructureComponent,
    FeeInstallmentsComponent
  ],
      imports: [

            BrowserModule,
            AppRoutingModule,
            BrowserAnimationsModule,

            MatDatepickerModule,
            MatMomentDateModule,
            MatCheckboxModule,
            MatInputModule,
            MatCardModule,
            MatButtonModule,
            MatSelectModule,
            MatCheckboxModule,
            MatRadioModule,
            FlexLayoutModule,
            MatFormFieldModule,
            ReactiveFormsModule,
            MatToolbarModule,
            MatStepperModule,
            MatTableModule,
            FormsModule,
            MatSortModule,
            MatIconModule,

      ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
