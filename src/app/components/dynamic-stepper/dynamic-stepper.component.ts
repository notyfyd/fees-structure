import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-dynamic-stepper',
  templateUrl: './dynamic-stepper.component.html',
  styleUrls: ['./dynamic-stepper.component.css']
})
export class DynamicStepperComponent implements OnInit {
      isLinear = false;
      formGroup : FormGroup;
      form: FormArray;
      constructor(private _formBuilder: FormBuilder) {
      }

      ngOnInit() {
            this.formGroup = this._formBuilder.group({
                  form : this._formBuilder.array([this.init()])
            });
            this.addItem();
      }

      init(){
            return this._formBuilder.group({
                  cont :new FormControl('', [Validators.required]),
            });
      }

      addItem(){
            this.form = this.formGroup.get('form') as FormArray;
            this.form.push(this.init());
      }
}
