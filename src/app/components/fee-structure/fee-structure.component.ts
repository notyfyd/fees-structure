import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-fee-structure',
  templateUrl: './fee-structure.component.html',
  styleUrls: ['./fee-structure.component.css']
})
export class FeeStructureComponent implements OnInit {
      
      form: FormGroup;


      constructor(private fb: FormBuilder) { }

      ngOnInit() {
            
            this.form =this.fb.group({
                  feeStructure: this.fb.array([this.addFeeGroup()])
            });
            
      }
      
      addFeeGroup() {
            return this.fb.group({
                  feeType: ['', Validators.required],
                  description: ['', Validators.required],
                  lateFee: ['', Validators.required]
            });
      }
      
      get feeStructureArray() {
            return <FormArray>this.form.get('feeStructure');
      }
      
      addFees(){
            this.feeStructureArray.push(this.addFeeGroup());
            
      }
      removeFees(index) {
            this.feeStructureArray.removeAt(index);
      }
      onSubmit() {
            console.log(this.form.value);
      }



}
