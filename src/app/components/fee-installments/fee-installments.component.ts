import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {MAT_DATE_FORMATS, MatTableDataSource} from '@angular/material';
import * as moment from 'moment';



export const DD_MM_YYYY_Format = {
      parse: {
            dateInput: 'LL',
      },
      display: {
            dateInput: 'DD/MM/YYYY'
            // monthYearLabel: 'MMM YYYY',
            // dateA11yLabel: 'LL',
            // monthYearA11yLabel: 'MMMM YYYY',
      },
};

export interface Installments {
      inst: String;
}



@Component({
      selector: 'app-fee-installments',
      templateUrl: './fee-installments.component.html',
      styleUrls: ['./fee-installments.component.css'],
      providers: [
            {provide: MAT_DATE_FORMATS, useValue: DD_MM_YYYY_Format},
            DatePipe
      ]
})
export class FeeInstallmentsComponent implements OnInit {

      inst: Installments[] = [
            {inst: 'One Time Payment'}, {inst: '1st Installment'},{inst: '2nd Installment'},{inst: '3rd Installment'},
            {inst: '4th Installment'},{inst: '5th Installment'},{inst: '6th Installment'},{inst: '7th Installment'},
            {inst: '8th Installment'},{inst: '9th Installment'},{inst: '10th Installment'}

      ];

      isLinear = false;
      formGroup : FormGroup;
      form: FormArray;
      displayedInstColumns = ['inst','dueDate', 'dueAmount'];


      instDataSource = new MatTableDataSource(this.inst);
      constructor(private _formBuilder: FormBuilder) {
      }
      ngOnInit() {
            this.formGroup = this._formBuilder.group({
                  form : this._formBuilder.array([this.init()])
            });
            this.addItem();
      }

      init(){
            return this._formBuilder.group({
                  cont :new FormControl('', [Validators.required]),
            });
      }

      addItem(){
            this.form = this.formGroup.get('form') as FormArray;
            this.form.push(this.init());
      }

  

}
