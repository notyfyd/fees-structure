import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeInstallmentsComponent } from './fee-installments.component';

describe('FeeInstallmentsComponent', () => {
  let component: FeeInstallmentsComponent;
  let fixture: ComponentFixture<FeeInstallmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeeInstallmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeeInstallmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
