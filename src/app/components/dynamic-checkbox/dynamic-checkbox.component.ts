import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {MatTableDataSource} from '@angular/material';

export interface GradeClass {
      
      str: string;
}

@Component({
  selector: 'app-dynamic-checkbox',
  templateUrl: './dynamic-checkbox.component.html',
  styleUrls: ['./dynamic-checkbox.component.css']
})
export class DynamicCheckboxComponent implements OnInit {

      grades: Array<String> =   ['Junior KG','Senior KG','1st Grade','2nd Grade', '3rd Grade',
            '4th Grade','5th Grade','6th Grade','7th Grade','8th Grade','9th Grade','10th Grade','11th Grade','12th Grade'];
      selectedGrades: Array<GradeClass>;
      displayedColumns = ['slno', 'class','A','B', 'C','D','E','F','G','H'];

      form: FormGroup;
      secondFormGroup: FormGroup;
      isLinear = true;
      dataSource;

      constructor(private fb: FormBuilder) { }

      ngOnInit() {
            this.form = this.fb.group({
                  schoolGrades: this.addGradesControl()
                  
            });

            this.secondFormGroup = this.fb.group({
               

            });
      }
      
      addGradesControl() {
            const arr = this.grades.map(res => {
                  return this.fb.control(false);
            });
            return this.fb.array(arr);
      }
      
      get gradesArray() {
            return <FormArray> this.form.get('schoolGrades');
      }
      
      getSelectedGradesValue() {
            this.selectedGrades = [];
            // this.selectedGrades = [];
            this.gradesArray.controls.forEach((control, i) => {
                  if(control.value) {
                        this.selectedGrades.push({str: this.grades[i].toString()});
                  }
            });
            
            this.dataSource = new MatTableDataSource(this.selectedGrades);

            
      }
      
      up() {
            this.getSelectedGradesValue();
      }
      
     

}
