import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-f-grades',
  templateUrl: './f-grades.component.html',
  styleUrls: ['./f-grades.component.css']
})
export class FGradesComponent implements OnInit {
      reactiveForm: FormGroup;
      states: Array<String> = ['AR', 'AL', 'CA', 'DC'];

      constructor(private fb: FormBuilder) { }

      ngOnInit() {
            this.reactiveForm = this.fb.group({
                  firstName: [''],
                  lastName: [''],
                  address: this.fb.group({
                        addressType: [],
                        expiryDate: [],
                        streetAddress:[],
                        city: [],
                        state: [this.states],
                        zipcode: ['', [Validators.required, Validators.pattern('^[0-9]{6}$')]]
                  })
                  
            });

      }
      login() {
            console.log(this.reactiveForm);
      }
      
      get zipcode () {
            const temp = <FormGroup>this.reactiveForm.controls.address;
            return temp.controls.zipcode;
      }
      



}
