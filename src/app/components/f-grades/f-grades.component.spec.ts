import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FGradesComponent } from './f-grades.component';

describe('FGradesComponent', () => {
  let component: FGradesComponent;
  let fixture: ComponentFixture<FGradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FGradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FGradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
